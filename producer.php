<?php

class Producer  {
    private $db;
    public function __construct()
    {
        $this->db = new PDO('mysql:host=localhost;dbname=test', "user", "password");

    }
    private function GetLastRow()
    {
        $query = $this->db->query('SELECT * FROM numbers ORDER BY numbers.id DESC');
        $rows = $query->fetchAll();
        if(count($rows)>0)
            return $rows[0];
        else
            return 0;
    }
    public function runProduce()
    {
        $rk = new RdKafka\Producer();
        $rk->setLogLevel(LOG_DEBUG);
        $rk->addBrokers("127.0.0.1");

        $topic = $rk->newTopic("bnmbnm");
        $lastRow=$this->GetLastRow();
        $currentIndex = $lastRow["id"];
        $i=0;
        while(true) {
            $lastRow=$this->GetLastRow();


            if($currentIndex<$lastRow["id"]) {
                $currentIndex = $lastRow["id"];

                $newValue = $lastRow["number"] + 1;
                $topic->produce(RD_KAFKA_PARTITION_UA, 0, $newValue);

                echo $newValue, "\n";
            }
            usleep(100000);
        }
    }

}
$producer = new Producer();
$producer->runProduce();







