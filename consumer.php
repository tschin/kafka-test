<?php



class Consumer  {
    private $db;
    public function __construct()
    {
        $this->db = new PDO('mysql:host=localhost;dbname=test', "user", "password");
    }
    public function runConsumer()
    {
        $query = $this->db->prepare("INSERT INTO numbers (number) VALUES (:number)");
        $query->execute(array('number' => 1));

        $rk = new RdKafka\Consumer();
        $rk->setLogLevel(LOG_DEBUG);
        $rk->addBrokers("127.0.0.1");

        $topicConf = new RdKafka\TopicConf();


        $topicConf->set('auto.commit.enable', 'true');
        $topicConf->set('auto.commit.interval.ms','100');

        $topic = $rk->newTopic("bnmbnm",$topicConf);

        $topic->consumeStart(0, RD_KAFKA_OFFSET_BEGINNING);

        $number=0;

        while (true) {
            $msg = $topic->consume(0,1000000);
            if ($msg->err) {
                echo $msg->errstr(), "\n";
                //break;
            } else {
                $number = $msg->payload;
                $query = $this->db->prepare('SELECT * FROM numbers WHERE number=:number');
                $query -> execute(array('number'=>$number));
                $rows=count($query->fetchAll());


                if($rows==0) {
                    $query = $this->db->prepare("INSERT INTO numbers (number) VALUES (:number)");
                    $query->execute(array('number' => $number));
                }
                echo  $msg->payload, "\n";
                echo $msg->offset, "offset \n";

                usleep(50000);

            }
        }
    }

}

$consumer = new Consumer();
$consumer->runConsumer();






